# Rédaction d'une nouvelle Pull Request

⚠️ Le titre de la pull request doit être suffisamment explicite pour que l'on ne cherche pas le sujet lié.

Commencer par : `Feat`, `Fix` ou `Refacto`, suivi d'un tiret ` - ` puis du titre

Ex: `FIX - applicant panel ne s'ouvre plus`
## Front

```md
### :octopus: USER STORIES :
- https://LIEN-CLICKUP (_sprint 14_)

**_Contexte_** :
Explication du pourquoi de la PR.


### PACKAGES :
- NOM_PACKAGE@0.0.0 [[NPM](https://www.npmjs.com/package/NOM_PACKAGE) | [GITHUB](https://github.com/USER/NOM_PACKAGE)]

### :heavy_plus_sign: UPDATES :
- Fix ouverture panel

### :interrobang: FORM VALIDATION & ERREURS :
| Route | Champ | Validations | Message d'erreur |
| --- | --- | --- | --- |
| Nom de la route | Nom du champ | ne peut pas être vide, 6 caractères min. | `Ce champ ne peut pas être vide` - `Le titre de votre offre doit contenir au minium 6 caractères` |

### :bus: ROUTES :

#### Routing _SECTION NAME_

:eyeglasses:  Explications si besoin

| Route | Description |
| --- | --- |
| `/#/route/123` | Explication de la route/spécifications |

### 🐧 DEV - HOW IT WORKS :
Explication si nécessaire

Expliquer comment le code marche, quelles sont les modifications impactantes et importantes

### 🐧 DEV - EXCEPTIONS :
- exception #1 à expliquer
- exception #2 à expliquer

### 🖼️ SCREENSHOTS :

#### Step bar responsive

##### :desktop:  Grande taille d'écran, css rework
<img src="/uploads/ce30e13a6057f99fb4e43d1116db6c61/Capture_d_écran_2021-12-12_à_16.14.44.png" width="500" height="auto" alt="Capture_d_écran_2021-12-12_à_16.14.44" />

```


## BACK

```md
### :octopus: USER STORIES :
- https://LIEN-CLICKUP (_sprint 14_)

**_Contexte_** :
Explication du pourquoi de la PR.


### PACKAGES :
- NOM_PACKAGE@0.0.0 [[NPM](https://www.npmjs.com/package/NOM_PACKAGE) | [GITHUB](https://github.com/USER/NOM_PACKAGE)]

### :heavy_plus_sign: UPDATES :
- Fix ouverture panel

### Migrations :
- `npx run migrate-up blabla.js`

### :interrobang: FORM VALIDATION & ERREURS :
| Route | Champ | Validations | Message d'erreur |
| --- | --- | --- | --- |
| **/#1/** | Titre offre| ne peut pas être vide, 6 caractères min. | `Ce champ ne peut pas être vide` - `Le titre de votre offre doit contenir au minium 6 caractères` |

### :bus: ROUTES :

#### Routing _SECTION NAME_

:eyeglasses:  Explications si besoin

| Route | Description |
| --- | --- |
| `/#/route/123` | Explication de la route/spécifications |

### 🐧 DEV - HOW IT WORKS :
Explication si nécessaire

Expliquer comment le code marche, quelles sont les modifications impactantes et importantes

### 🐧 DEV - EXCEPTIONS :
- exception #1 à expliquer
- exception #2 à expliquer

```